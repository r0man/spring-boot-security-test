package ru.edu.repository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import ru.edu.entity.Product;
import ru.edu.exception.NotFoundException;

@DataJpaTest
class ProductRepositoryTest {

    @Autowired
    private ProductRepository repository;

    @Test
    @Sql("classpath:products.sql")
    void getProductByName() {
        String productName = "lime";
        Product product = repository.getProductByName(productName).orElseThrow(NotFoundException::new);
        Assertions.assertThat(product.getName()).isEqualTo(productName);
    }
}
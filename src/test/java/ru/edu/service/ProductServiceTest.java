package ru.edu.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.edu.entity.Product;
import ru.edu.exception.NotFoundException;
import ru.edu.repository.ProductRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository repository;

    private ProductService service;

    @BeforeEach
    void setUp() {
        service = new ProductService(repository);
    }

    @Test
    public void getProductPriceWithDiscount() {
        Product apple = new Product();
        apple.setPrice(10.5);
        apple.setQuantity(100);

        when(repository.getProductByName(anyString())).thenReturn(Optional.of(apple));
        double price = service.getProductPriceWithDiscount("apple", 10);
        assertThat(price).isEqualTo(945.0);
    }

    @Test
    void getProductPriceWithDiscountException() {
        String product = "line";
        Assertions.assertThatThrownBy(() -> service.getProductPriceWithDiscount(product, 10))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("product " + product + " not found");
    }


}
insert into USERS(name, email, password) values
    ( 'admin', 'admin@gmail.com', '$2a$10$IFZ8J3Y5KbH8bc71kJaL2OY8qwR2H3DCTEpCD5zATucWHd6H959Ym'),
    ( 'Petr Petrov', 'petrov@gmail.com', '$2a$10$IFZ8J3Y5KbH8bc71kJaL2OY8qwR2H3DCTEpCD5zATucWHd6H959Ym');

insert into ROLES (NAME) values ('ROLE_ADMIN'), ('ROLE_MANAGER');
insert into USERS_ROLES values(1, 1), (2, 2);

insert into products(name, price, quantity) values ('apple', 10.5, 100), ('orange', 30.0, 50);
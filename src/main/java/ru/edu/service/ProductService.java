package ru.edu.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.edu.entity.Product;
import ru.edu.exception.NotFoundException;
import ru.edu.repository.ProductRepository;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductRepository repository;

    public double getProductPriceWithDiscount(String product, int discount) {
        Product productInStock = repository.getProductByName(product).orElseThrow(() -> new NotFoundException("product " + product + " not found"));
        double totalPrice = productInStock.getPrice() * productInStock.getQuantity();
        double discountPrice = totalPrice - totalPrice * discount / 100;
        return discountPrice > 0 ? discountPrice : totalPrice;
    }
}

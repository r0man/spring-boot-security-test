package ru.edu.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
@AllArgsConstructor
public class HomeController {

    @GetMapping("/login")
    public String login(@RequestParam(required = false) Optional<String> error, Model model) {
        error.ifPresent(e -> model.addAttribute("error", "Invalid Credentials provided"));
        return "login";
    }
}

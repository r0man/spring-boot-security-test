package ru.edu.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.service.ProductService;

@Controller
@RequestMapping(value = "/admin")
@AllArgsConstructor
public class AdminController {

    private final ProductService service;

    @GetMapping("/price")
    public String home(Model model) {
        model.addAttribute("price", service.getProductPriceWithDiscount("apple", 10));
        return "home";
    }
}
